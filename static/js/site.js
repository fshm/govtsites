function newTableRow(sitename, table) {
	row = table.insertRow();

	cell1 = row.insertCell(0);
	cell2 = row.insertCell(1);

	cell1.innerHTML = "<a href='" + sitename + "' target='_blank'>" + sitename + "</a>";
	cell2.innerText = "checking";

	row.id = sitename;
}

function loadDataFromJSON() {
	var table = document.getElementById('site-table');

	for (i=0; i<sites.length; i++) {
		newTableRow(sites[i], table);
	}
}

function performAJAX(sitename) {

	function responseCallback() {

		site_status = "not working";
		target_row = document.getElementById(sitename);

		if (this.status === 200) {
			site_status = "working";
			target_row.children[1].classList.remove("not-working");
			target_row.children[1].classList.add("working");
		} else {
			target_row.children[1].classList.remove("working");
			target_row.children[1].classList.add("not-working");
		}
		
		target_row.children[1].innerText = site_status;
	}

	xhr = new XMLHttpRequest();
	xhr.addEventListener("load", responseCallback);
	xhr.open("HEAD", "https://cors-anywhere.herokuapp.com/"+sitename);
	xhr.timeout = 180000;
	xhr.setRequestHeader("x-requested-with", sitename.split("//")[1]);

	xhr.ontimeout = function (e) {
		target_row = document.getElementById(sitename);
		target_row.children[1].classList.remove("working");
		target_row.children[1].classList.add("not-working");
		target_row.children[1].innerText = "not working";
	}

	xhr.send();
}

function checkSites() {

	if (navigator.onLine) {
		for (i=0; i<sites.length; i++){
			target_row = document.getElementById(sites[i]);
			target_row.children[1].innerText = "checking";
			performAJAX(sites[i]);
		}
	} else {

	}
}

